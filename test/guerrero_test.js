var expect = require('chai').expect;

let Arquero = require("../model/Arquero");
let Infanteria =require("../model/Infanteria");
let Madera =require("../model/madera");
let Hierro =require("../model/hierro");
let Fuego =require("../model/fuego");

describe("Guerrero Tests",function(){
    it("Obtener daño de Guerrero(infanteria) basico igual a 10",function () {
        let infanteria = new Infanteria();
        expect(infanteria.obtenerDaño()).equal(10);
    });

    it("Obtener daño de Guerrero(arquero) basico igual a 5",function () {
        let arquero = new Arquero();
        expect(arquero.obtenerDaño()).equal(5);
    });

    it("Obtener daño de Guerrero(infanteria) con un material",function () {
        let infanteria = new Infanteria();
        let armaMadera=new Madera(infanteria);
        let armaHierro=new Hierro(infanteria);
        let armaFuego=new Fuego(infanteria);
        expect(armaMadera.obtenerDaño()).equal(15);
        expect(armaHierro.obtenerDaño()).equal(20);
        expect(armaFuego.obtenerDaño()).equal(40);
    });

    it("Obtener daño de Guerrero(Arquero) con un material",function () {
        let arquero = new Arquero();
        let armaMadera=new Madera(arquero);
        let armaHierro=new Hierro(arquero);
        let armaFuego=new Fuego(arquero);
        expect(armaMadera.obtenerDaño()).equal(10);
        expect(armaHierro.obtenerDaño()).equal(15);
        expect(armaFuego.obtenerDaño()).equal(35);
    });

    it("Obtener daño de Guerrero(infanteria) con 2 materiales",function () {
        let infanteria = new Infanteria();
        let armaMadera=new Madera(infanteria);
        let armaHierro=new Hierro(infanteria);

        let armaMaderaHierro=new Hierro(armaMadera);
        let armaMaderaFuego=new Fuego(armaMadera);
        let armaHierroFuego=new Fuego(armaHierro);
        
        expect(armaMaderaHierro.obtenerDaño()).equal(25);
        expect(armaMaderaFuego.obtenerDaño()).equal(45);
        expect(armaHierroFuego.obtenerDaño()).equal(50);
        
    });

    it("Obtener daño de Guerrero(Arquero) con 2 materiales",function () {
        let arquero = new Arquero();
        let armaMadera=new Madera(arquero);
        let armaHierro=new Hierro(arquero);

        let armaMaderaHierro=new Hierro(armaMadera);
        let armaMaderaFuego=new Fuego(armaMadera);
        let armaHierroFuego=new Fuego(armaHierro);
        expect(armaMaderaHierro.obtenerDaño()).equal(20);
        expect(armaMaderaFuego.obtenerDaño()).equal(40);
        expect(armaHierroFuego.obtenerDaño()).equal(45);
    });

    it("Obtener daño de Guerrero(infanteria) con 3 materiales",function () {
        let infanteria = new Infanteria();
        let armaMadera=new Madera(infanteria);
        let armaMaderaHierro=new Hierro(armaMadera);
        let armaMaderaHierroFuego=new Fuego(armaMaderaHierro);
        
        expect(armaMaderaHierroFuego.obtenerDaño()).equal(55);
    });
    
    it("Obtener daño de Guerrero(Arquero) con 3 materiales",function () {
        let arquero = new Arquero();
        let armaHierro=new Hierro(arquero);
        let armaHierroFuego=new Fuego(armaHierro);
        let armaHierroFuegoMadera=new Madera(armaHierroFuego);
        
        expect(armaHierroFuegoMadera.obtenerDaño()).equal(50);
    });
});