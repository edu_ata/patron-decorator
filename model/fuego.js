class Fuego {
    constructor(guerrero) {
        this.materialFuego = 30;
        this.guerrero = guerrero;
    }
    obtenerDaño() {
        return this.materialFuego + this.guerrero.obtenerDaño();
    }
}

module.exports = Fuego;
