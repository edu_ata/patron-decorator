class Hierro {
    constructor(guerrero) {
        this.materialHierro = 10;
        this.guerrero = guerrero;
    }
    obtenerDaño() {
        return this.materialHierro + this.guerrero.obtenerDaño();
    }
}

module.exports = Hierro;
